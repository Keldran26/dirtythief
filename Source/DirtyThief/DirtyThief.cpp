// Copyright Epic Games, Inc. All Rights Reserved.

#include "DirtyThief.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DirtyThief, "DirtyThief" );
