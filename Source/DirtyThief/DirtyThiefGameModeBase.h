// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DirtyThiefGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIRTYTHIEF_API ADirtyThiefGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
